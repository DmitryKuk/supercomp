from typing import Union

import numpy


def u_t0(x: Union[numpy.ndarray, float]) -> numpy.ndarray:
    return numpy.sin(x)


def u_l0(t: float) -> float:
    return 0


def u_lmax(t: float) -> float:
    return 2
