import enum

import numpy

from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtGui import QColor, QBrush, QPen, QPixmap, QPainter
from PyQt5.QtWidgets import QGraphicsScene


class _State(enum.IntEnum):
    UNKNOWN = 0
    NOT_READY = 1
    IN_PROGRESS = 2
    READY = 3


class StateScene(QGraphicsScene):
    __color_by_state = {
        _State.UNKNOWN: Qt.white,
        _State.NOT_READY: Qt.red,
        _State.IN_PROGRESS: Qt.yellow,
        _State.READY: Qt.green
    }

    def __init__(self, l_n: int, t_n: int, parent=None) -> None:
        super().__init__(parent=parent)

        assert l_n > 0
        assert t_n > 0

        # Add field pixmap
        self.__field_pixmap = QPixmap(l_n, t_n)
        self.__field_pixmap.fill(self.__color_by_state[_State.NOT_READY])

        painter = QPainter(self.__field_pixmap)
        painter.setBrush(self.__color_by_state[_State.UNKNOWN])
        painter.setPen(Qt.NoPen)
        painter.drawRect(0, t_n, l_n, t_n)

        self.__field_pixmap_item = self.addPixmap(self.__field_pixmap)

        # Add highlighter
        self.__t_rect = self.addRect(-1, 0, l_n + 2, 3, QPen(Qt.NoPen), QBrush(QColor(128, 128, 128, 128)))

    @pyqtSlot(int)
    def set_current_t(self, t: int) -> None:
        rect = self.__t_rect.rect()
        rect.moveTop(t)
        self.__t_rect.setRect(rect)

    @pyqtSlot(int, int, int)
    def set_data_in_progress(self, l_start: int, l_end: int, t_start: int) -> None:
        painter = QPainter(self.__field_pixmap)
        painter.setBrush(self.__color_by_state[_State.IN_PROGRESS])
        painter.setPen(Qt.NoPen)
        painter.drawRect(l_start, t_start, l_end - l_start, 1)
        painter.end()

        self.__redraw_field_pixmap()

    @pyqtSlot(int, int, numpy.ndarray)
    def set_data(self, l_start: int, t_start: int, data: numpy.ndarray) -> None:
        painter = QPainter(self.__field_pixmap)
        painter.setBrush(self.__color_by_state[_State.READY])
        painter.setPen(Qt.NoPen)
        painter.drawRect(l_start, t_start, data.shape[0], 1)
        painter.end()

        self.__redraw_field_pixmap()

    def __redraw_field_pixmap(self) -> None:
        self.removeItem(self.__field_pixmap_item)
        self.__field_pixmap_item = self.addPixmap(self.__field_pixmap)
