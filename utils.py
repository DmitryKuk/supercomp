from typing import Tuple, List


def split_for_workers(*, workers: int, l_start: int = 0, l_end: int) -> List[Tuple[int, int]]:
    workers = min(workers, l_end - l_start)

    large_chunks = (l_end - l_start) % workers
    chunk_size = (l_end - l_start) // workers
    small_chunks_offset = (chunk_size + 1) * large_chunks

    ls = [
        (l_start + i * (chunk_size + 1), l_start + (i + 1) * (chunk_size + 1))
        for i in range(large_chunks)
    ]
    ls.extend(
        (l_start + i * chunk_size + small_chunks_offset, l_start + (i + 1) * chunk_size + small_chunks_offset)
        for i in range(workers - large_chunks)
    )
    return ls


# if __name__ == '__main__':
#     for (workers, l_start, l_end) in [
#         (1, 0, 1), (1, 0, 5), (1, 0, 6), (5, 0, 5), (5, 0, 6), (5, 0, 7), (5, 0, 8), (5, 0, 9),
#         (1, 1, 5), (1, 1, 6), (5, 1, 5), (5, 1, 6), (5, 1, 7), (5, 1, 8), (5, 1, 9),
#         (1, 2, 1), (1, 2, 5), (1, 2, 6), (5, 2, 5), (5, 2, 6), (5, 2, 7), (5, 2, 8), (5, 2, 9)
#     ]:
#         print(f'({workers}, {l_start}..{l_end}): {split_for_workers(workers=workers, l_start=l_start, l_end=l_end)}')
