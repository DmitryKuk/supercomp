import numpy


class ResultField:
    def __init__(self, l_n: int, t_n: int) -> None:
        self.__values = numpy.empty((t_n + 1, l_n + 1))
        self.__values.fill(numpy.nan)

    def data(self, t: int) -> numpy.ndarray:
        return self.__values[t]

    def set_data(self, l_start: int, t: int, data: numpy.ndarray) -> None:
        self.__values[t, l_start:l_start + data.shape[0]] = data
