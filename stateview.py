from PyQt5.QtWidgets import QGraphicsView


class StateView(QGraphicsView):
    def showEvent(self, event):
        scene = self.scene()
        if scene is not None:
            self.fitInView(scene.sceneRect())
        return super().showEvent(event)

    def resizeEvent(self, event):
        scene = self.scene()
        if scene is not None:
            self.fitInView(scene.sceneRect())
        return super().resizeEvent(event)
