from typing import List
import argparse
import logging

from PyQt5.QtWidgets import QApplication

from mainwindow import MainWindow
from calculatormanager import CalculatorManager


logger = logging.getLogger(__name__)


class Application(QApplication):
    def __init__(self, argv: List[str]) -> None:
        self.__d: float = None

        self.__l_max: float = None
        self.__l_n: int = None
        self.__h: float = None

        self.__t_max: float = None
        self.__t_n: int = None
        self.__s: float = None

        self.__threads: int = None
        self.__use_shared_memory: bool = None

        self.__parse_args(argv)

        super().__init__(argv)

        self.__calculation_manager = CalculatorManager(
            d=self.__d, l_start=0, l_end=self.__l_n, l_n=self.__l_n, h=self.__h, t_end=self.__t_n, s=self.__s,
            threads=self.__threads,
            use_shared_memory=self.__use_shared_memory
        )

        self.__main_window = MainWindow(
            d=self.__d,

            l_max=self.__l_max,
            l_n=self.__l_n,
            h=self.__h,

            t_max=self.__t_max,
            t_n=self.__t_n,
            s=self.__s,

            threads=self.__threads,
            use_shared_memory=self.__use_shared_memory
        )

        self.__main_window.start.connect(self.__calculation_manager.start)

        self.__calculation_manager.data_in_progress.connect(self.__main_window.set_data_in_progress)
        self.__calculation_manager.data_ready.connect(self.__main_window.set_data)

    def exec(self) -> int:
        self.__main_window.show()
        return super().exec()

    def __parse_args(self, args) -> None:
        parser = argparse.ArgumentParser(prog=args[0])

        parser.add_argument('--D', dest='d', type=float, nargs='?', default=10.0)
        parser.add_argument('--Lmax', dest='l_max', type=float, nargs='?', default=10.0)
        parser.add_argument('--Ln', dest='l_n', type=int, nargs='?', default=10)
        parser.add_argument('--Tmax', dest='t_max', type=float, nargs='?', default=10.0)
        parser.add_argument('--Tn', dest='t_n', type=int, nargs='?', default=1000)
        parser.add_argument('--threads', type=int, nargs='?', default=1)
        parser.add_argument('--use-shared-memory', dest='use_shared_memory', action='store_true')

        parsed_args = parser.parse_args(args[1:])

        self.__d = parsed_args.d
        self.__l_max = parsed_args.l_max
        self.__l_n = parsed_args.l_n
        self.__t_max = parsed_args.t_max
        self.__t_n = parsed_args.t_n
        self.__threads = parsed_args.threads
        self.__use_shared_memory = parsed_args.use_shared_memory

        assert self.__l_max > 0
        assert self.__l_n > 0
        assert self.__t_max > 0
        assert self.__t_n > 0
        assert self.__threads > 0

        self.__h = self.__l_max / self.__l_n
        self.__s = self.__t_max / self.__t_n
        assert 2 * self.__d * self.__s <= self.__h ** 2
