from typing import List
import sys
import logging

from application import Application


def main(argv: List[str]) -> int:
    return Application(argv).exec()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    sys.exit(main(sys.argv))
