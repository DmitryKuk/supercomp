import numpy

from PyQt5.QtCore import pyqtSlot
from qwt import QwtPlot, QwtPlotGrid, QwtPlotCurve


class Plot(QwtPlot):
    def __init__(self, l_n: int, h: float, *args) -> None:
        super().__init__(*args)

        self.__grid = QwtPlotGrid()
        self.__grid.attach(self)

        self.__grid.enableX(True)
        self.__grid.enableY(True)
        self.__grid.enableXMin(True)

        self.enableAxis(QwtPlot.xBottom)
        self.enableAxis(QwtPlot.yLeft)

        self.__curve = QwtPlotCurve()
        self.__curve.attach(self)

        self.__xs = numpy.arange(l_n + 1) * h

    @pyqtSlot(numpy.ndarray)
    def set_series(self, data: numpy.ndarray) -> None:
        self.__curve.setData(self.__xs, data)
        self.replot()
        self.repaint()
