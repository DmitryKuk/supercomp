import os
import inspect

import numpy

from PyQt5 import uic
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt
from PyQt5.QtWidgets import QMainWindow

from plot import Plot
from stateview import StateView
from statescene import StateScene
from resultfield import ResultField


class MainWindow(QMainWindow):
    start = pyqtSignal()

    def __init__(
            self,
            *,

            d: float,

            l_max: float,
            l_n: int,
            h: float,

            t_max: float,
            t_n: int,
            s: float,

            threads: int,
            use_shared_memory: bool,

            parent=None
    ) -> None:
        super().__init__(parent=parent)

        uic.loadUi(
            os.path.join(os.path.dirname(inspect.getabsfile(inspect.currentframe())), "ui", "mainwindow.ui"),
            self
        )

        self.__result_field = ResultField(l_n=l_n, t_n=t_n)

        self.__plot = Plot(l_n=l_n, h=h)
        self.plot_group_box.layout().addWidget(self.__plot)

        self.__plot.setAxisScale(Plot.yLeft, -1, 1)
        self.__plot.setAxisAutoScale(Plot.yLeft, True)

        self.__plot.setAxisScale(Plot.xBottom, 0, l_n)

        self.t_i_spin_box.setMaximum(t_n - 1)
        self.t_i_scroll_bar.setMaximum(t_n - 1)
        self.t_i_scroll_bar.sliderMoved.connect(self.t_i_spin_box.setValue)
        self.t_i_spin_box.valueChanged.connect(self.t_i_scroll_bar.setValue)

        self.__state_scene = StateScene(l_n=l_n, t_n=t_n)
        self.__state_view = StateView()
        self.ready_group_box.layout().insertWidget(0, self.__state_view)
        self.__state_view.setScene(self.__state_scene)
        self.__state_scene.sceneRectChanged.connect(self.__state_view.fitInView)
        self.t_i_spin_box.valueChanged.connect(self.__state_scene.set_current_t)
        self.t_i_spin_box.valueChanged.connect(self.__plot_series)
        self.t_i_spin_box.valueChanged.connect(lambda t_i: self.t_view_line_edit.setText(format(t_i / t_n, ".4f")))
        self.t_view_line_edit.setText(format(0, ".4f"))

        self.d_line_edit.setText(format(d, ".4f"))
        self.l_line_edit.setText(format(l_max))
        self.l_n_line_edit.setText(str(l_n))
        self.h_line_edit.setText(format(h, ".4f"))
        self.t_line_edit.setText(format(t_max, ".4f"))
        self.t_n_line_edit.setText(str(t_n))
        self.s_line_edit.setText(format(s, ".4f"))

        self.threads_line_edit.setText(str(threads))
        self.use_shared_memory_check_box.setCheckState(Qt.Checked if use_shared_memory else Qt.Unchecked)

        self.start_push_button.clicked.connect(self.start.emit)
        self.start_push_button.clicked.connect(lambda: self.start_push_button.setEnabled(False))

    @pyqtSlot(int, int, int)
    def set_data_in_progress(self, l_start: int, l_end: int, t_start: int) -> None:
        self.__state_scene.set_data_in_progress(l_start=l_start, l_end=l_end, t_start=t_start)

    @pyqtSlot(int, int, numpy.ndarray)
    def set_data(self, l_start: int, t: int, data: numpy.ndarray) -> None:
        self.__result_field.set_data(l_start=l_start, t=t, data=data)
        self.__state_scene.set_data(l_start=l_start, t_start=t, data=data)

    @pyqtSlot(int)
    def __plot_series(self, t: int) -> None:
        self.__plot.set_series(self.__result_field.data(t))
