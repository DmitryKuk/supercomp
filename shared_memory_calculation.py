from typing import Optional, Tuple, Sequence, Callable
import threading
import functools

import numpy

import utils
from calculator import make_calculator


def calculate_using_shared_memory(
        *,

        # Material
        d: float,

        # Size
        l_start: int,
        l_end: int,
        l_n: int,
        h: float,

        # Time
        t_end: int,
        s: float,

        # Threads
        threads: int = 1,

        # Notifications
        on_data_in_progress: Callable[[int, int, int], None],
        on_data_ready: Callable[[int, int, int, numpy.ndarray], None]
) -> None:
    chunks = utils.split_for_workers(workers=threads, l_start=l_start, l_end=l_end)

    def make_ready_semaphores() -> \
            Sequence[
                Tuple[
                    Optional[threading.Semaphore],
                    Optional[threading.Semaphore],
                    threading.Semaphore,
                    threading.Semaphore
                ]
            ]:
        semaphores_for_left = tuple(threading.Semaphore() for _ in range(len(chunks)))
        semaphores_for_right = tuple(threading.Semaphore() for _ in range(len(chunks)))
        left_semaphores = (None,) + semaphores_for_right[:-1]
        right_semaphores = semaphores_for_left[1:] + (None,)
        return tuple(zip(left_semaphores, right_semaphores, semaphores_for_left, semaphores_for_right))

    data_current = numpy.empty(l_n)
    data_previous = numpy.empty(l_n)

    workers = [
        threading.Thread(
            target=functools.partial(
                __calculate_chunks_using_shared_memory,
                d=d, l_start=l_start, l_end=l_end, l_n=l_n, h=h, t_end=t_end, s=s,
                data_current=data_current, data_previous=data_previous,
                left_semaphore=rs[0], right_semaphore=rs[1], semaphore_for_left=rs[2], semaphore_for_right=rs[3],
                on_data_in_progress=on_data_in_progress, on_data_ready=on_data_ready
            )
        )
        for (l_start, l_end), rs in zip(chunks, make_ready_semaphores())
    ]

    for worker in workers:
        worker.start()

    for worker in workers:
        worker.join()


def __calculate_chunks_using_shared_memory(
        *,

        # Material
        d: float,

        # Size
        l_start: int,
        l_end: int,
        l_n: int,
        h: float,

        # Time
        t_end: int,
        s: float,

        # Memory
        data_current: numpy.ndarray,
        data_previous: numpy.ndarray,

        # Synchronization
        left_semaphore: Optional[threading.Semaphore],  # Acquire
        right_semaphore: Optional[threading.Semaphore],  # Acquire
        semaphore_for_left: threading.Semaphore,  # Release
        semaphore_for_right: threading.Semaphore,  # Release

        # Notifications
        on_data_in_progress: Callable[[int, int, int], None],
        on_data_ready: Callable[[int, int, numpy.ndarray], None]
) -> None:
    calculator = make_calculator(
        d=d,
        l_start=l_start, l_end=l_end, l_n=l_n, h=h,
        t_end=t_end, s=s,
        data_current=data_current, data_previous=data_previous
    )

    # Here all semaphores has value 1 => don't need to release

    try:
        t = 0
        while True:
            # TODO: Uncomment to see yellow lines.
            # on_data_in_progress(l_start, l_end, t)

            # Wait previous
            if left_semaphore is not None:
                left_semaphore.acquire()
            if right_semaphore is not None:
                right_semaphore.acquire()

            data = next(calculator)

            # Allow left and right neighbours to continue
            if left_semaphore is not None:
                semaphore_for_left.release()
            if right_semaphore is not None:
                semaphore_for_right.release()

            on_data_ready(l_start, t, data)

            t += 1
    except StopIteration:
        pass


# def do_calculate():
#     def on_data_in_progress(l_start: int, l_end: int, t: int) -> None:
#         print(f'on_data_in_progress(l_start={l_start}, l_end={l_end}, t={t})')
#
#     def on_data_ready(l_start: int, t: int, block_data: numpy.ndarray) -> None:
#         print(f'on_data_ready(l_start={l_start}, t={t}, data={list(block_data)})')
#
#     calculate_using_shared_memory(
#         d=1.0, l_start=0, l_end=10, l_n=10, h=1.0, t_end=5, s=1.0, threads=3,
#         on_data_in_progress=on_data_in_progress, on_data_ready=on_data_ready
#     )
#
#
# if __name__ == '__main__':
#     do_calculate()
