# Installation

```
python3 -m pip install -r requirements.txt
```

# Launch

```
python3 __main__.py --Lmax=100 --Ln=100 --Tmax=50 --Tn=10000 --threads=3 --use-shared-memory
```
