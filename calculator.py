from typing import Optional, Generator, Tuple

import numpy

import function


def make_calculator(
        *,

        # Material
        d: float,

        # Size
        l_start: int,
        l_end: int,
        l_n: int,
        h: float,

        # Time
        t_end: int,
        s: float,

        # Optional shared memory
        data_current: Optional[numpy.ndarray] = None,
        data_previous: Optional[numpy.ndarray] = None
) -> Generator[numpy.ndarray, Optional[Tuple[Optional[float], Optional[float]]], None]:
    assert (data_current is None and data_previous is None) or (data_current is not None and data_previous is not None)

    if data_current is None:
        current = numpy.empty(l_end - l_start)
        previous = numpy.empty(l_end - l_start)
    else:
        current = data_current[l_start:l_start + l_end - l_start]
        previous = data_previous[l_start:l_start + l_end - l_start]

    extra_left: Optional[float] = None
    extra_right: Optional[float] = None

    k1 = s * d / (h * h)
    k2 = (1 - 2 * k1)

    def calculate_start_chunk() -> None:
        # Middle part
        current[1:-1] = function.u_t0(numpy.arange((l_start + 1), (l_end - 1)) * h)

        # Left bound
        if l_start == 0:
            current[0] = function.u_l0(0.0)
        else:
            current[0] = function.u_t0(l_start * h)

        # Right bound
        if l_end == l_n:
            current[-1] = function.u_lmax(0.0)
        else:
            current[-1] = function.u_t0((l_end - 1) * h)

    def calculate_chunk(t: float) -> None:
        # Middle part
        current[1:-1] = k1 * (previous[:-2] + previous[2:]) + k2 * previous[1:-1]

        # Left bound
        if l_start == 0:
            assert extra_left is None
            current[0] = function.u_l0(t)
        else:
            assert extra_left is not None
            current[0] = k1 * (extra_left + previous[1]) + k2 * previous[0]

        # Right bound
        if l_end == l_n:
            assert extra_right is None
            current[-1] = function.u_lmax(t)
        else:
            assert extra_right is not None
            current[-1] = k1 * (previous[-2] + extra_right) + k2 * previous[-1]

    def get_extra_elements_if_need() -> None:
        nonlocal extra_left, extra_right
        if data_current is None:
            extra_left, extra_right = args
        else:
            assert args is None
            if l_start > 0:
                extra_left = data_previous[l_start - 1]

            if l_end < l_n:
                extra_right = data_previous[l_end]

    def swap_current_previous() -> None:
        nonlocal current, previous, data_current, data_previous
        current, previous = previous, current
        data_current, data_previous = data_previous, data_current

    calculate_start_chunk()
    args = yield current
    for t_i in range(1, t_end):
        swap_current_previous()
        get_extra_elements_if_need()
        calculate_chunk(t=t_i * s)
        args = yield current


# Simple demo:
# def do_calculate1():
#     c1 = make_calculator(d=1.0, l_start=0, l_end=5, l_n=10, h=1.0, t_end=5, s=1.0)
#     c2 = make_calculator(d=1.0, l_start=5, l_end=10, l_n=10, h=1.0, t_end=5, s=1.0)
#
#     d1, d2 = c1.send(None), c2.send(None)
#     print(list(d1), list(d2))
#     for _ in range(4):
#         er, el = d2[0], d1[-1]
#         d1, d2 = c1.send((None, d2[0])), c2.send((d1[-1], None))
#         print(list(d1), list(d2), f'{er}, {el} => {d2[0]}, {d1[-1]}')
#
#
# def do_calculate2():
#     data_current = numpy.empty(10)
#     data_previous = numpy.empty(10)
#
#     c1 = make_calculator(
#         d=1.0, l_start=0, l_end=5, l_n=10, h=1.0, t_end=5, s=1.0,
#         data_current=data_current, data_previous=data_previous
#     )
#     c2 = make_calculator(
#         d=1.0, l_start=5, l_end=10, l_n=10, h=1.0, t_end=5, s=1.0,
#         data_current=data_current, data_previous=data_previous
#     )
#
#     d1, d2 = c1.send(None), c2.send(None)
#     print(list(d1), list(d2))
#     for _ in range(4):
#         er, el = d2[0], d1[-1]
#         d1, d2 = c1.send(None), c2.send(None)
#         print(list(d1), list(d2), f'{er}, {el} => {d2[0]}, {d1[-1]}')
#
#
# if __name__ == '__main__':
#     do_calculate1()
#     print('---')
#     do_calculate2()
