from typing import Optional, Tuple, Sequence, Callable
import threading
import queue
import functools

import numpy

import utils
from calculator import make_calculator


def calculate_using_copy(
        *,

        # Material
        d: float,

        # Size
        l_start: int,
        l_end: int,
        l_n: int,
        h: float,

        # Time
        t_end: int,
        s: float,

        # Threads
        threads: int = 1,

        # Notifications
        on_data_in_progress: Callable[[int, int, int], None],
        on_data_ready: Callable[[int, int, numpy.ndarray], None]
) -> None:
    chunks = utils.split_for_workers(workers=threads, l_start=l_start, l_end=l_end)

    def make_queues() -> \
            Sequence[
                Tuple[
                    Optional[queue.Queue],
                    Optional[queue.Queue],
                    queue.Queue,
                    queue.Queue
                ]
            ]:
        queues_for_left = tuple(queue.Queue() for _ in range(len(chunks)))
        queues_for_right = tuple(queue.Queue() for _ in range(len(chunks)))
        left_queues = (None,) + queues_for_right[:-1]
        right_queues = queues_for_left[1:] + (None,)
        return tuple(zip(left_queues, right_queues, queues_for_left, queues_for_right))

    workers = [
        threading.Thread(
            target=functools.partial(
                __calculate_chunks_using_copy,
                d=d, l_start=l_start, l_end=l_end, l_n=l_n, h=h, t_end=t_end, s=s,
                left_queue=q[0], right_queue=q[1], queue_for_left=q[2], queue_for_right=q[3],
                on_data_in_progress=on_data_in_progress, on_data_ready=on_data_ready
            )
        )
        for (l_start, l_end), q in zip(chunks, make_queues())
    ]

    for worker in workers:
        worker.start()

    for worker in workers:
        worker.join()


def __calculate_chunks_using_copy(
        *,

        # Material
        d: float,

        # Size
        l_start: int,
        l_end: int,
        l_n: int,
        h: float,

        # Time
        t_end: int,
        s: float,

        # Synchronization
        left_queue: Optional[queue.Queue],  # Get
        right_queue: Optional[queue.Queue],  # Get
        queue_for_left: queue.Queue,  # Put
        queue_for_right: queue.Queue,  # Put

        # Notifications
        on_data_in_progress: Callable[[int, int, int], None],
        on_data_ready: Callable[[int, int, numpy.ndarray], None]
) -> None:
    calculator = make_calculator(
        d=d,
        l_start=l_start, l_end=l_end, l_n=l_n, h=h,
        t_end=t_end, s=s
    )

    # Another threads don't need to receive any data for first line calculation

    extra_left, extra_right = None, None
    args = None

    try:
        t = 0
        while True:
            # TODO: Uncomment to see yellow lines.
            # on_data_in_progress(l_start, l_end, t)

            data = calculator.send(args)

            # Allow left and right neighbours to continue
            if left_queue is not None:
                queue_for_left.put(data[0])
            if right_queue is not None:
                queue_for_right.put(data[-1])

            on_data_ready(l_start, t, data)

            # Wait previous
            if left_queue is not None:
                extra_left = left_queue.get()
            if right_queue is not None:
                extra_right = right_queue.get()
            args = extra_left, extra_right

            t += 1
    except StopIteration:
        pass


# def do_calculate():
#     def on_data_in_progress(l_start: int, l_end: int, t: int) -> None:
#         print(f'on_data_in_progress(l_start={l_start}, l_end={l_end}, t={t})')
#
#     def on_data_ready(l_start: int, t: int, block_data: numpy.ndarray) -> None:
#         print(f'on_data_ready(l_start={l_start}, t={t}, data={list(block_data)})')
#
#     calculate_using_copy(
#         d=1.0, l_start=0, l_end=10, l_n=10, h=1.0, t_end=5, s=1.0, threads=3,
#         on_data_in_progress=on_data_in_progress, on_data_ready=on_data_ready
#     )
#
#
# if __name__ == '__main__':
#     do_calculate()
