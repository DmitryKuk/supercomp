import numpy

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QThread

from copy_calculation import calculate_using_copy
from shared_memory_calculation import calculate_using_shared_memory


class CalculatorManager(QThread):
    data_in_progress = pyqtSignal(int, int, int)
    data_ready = pyqtSignal(int, int, numpy.ndarray)

    def __init__(
            self,
            *,

            # Material
            d: float,

            # Size
            l_start: int,
            l_end: int,
            l_n: int,
            h: float,

            # Time
            t_end: int,
            s: float,

            # Threads
            threads: int,

            # Mode
            use_shared_memory: bool,

            parent=None
    ) -> None:
        super().__init__(parent=parent)

        self.__d = d

        self.__l_start = l_start
        self.__l_end = l_end
        self.__l_n = l_n
        self.__h = h

        self.__t_end = t_end
        self.__s = s

        self.__threads = threads

        self.__use_shared_memory = use_shared_memory

    @pyqtSlot()
    def run(self) -> None:
        calculate_fn = calculate_using_shared_memory if self.__use_shared_memory else calculate_using_copy

        calculate_fn(
            d=self.__d,
            l_start=self.__l_start, l_end=self.__l_end, l_n=self.__l_n, h=self.__h,
            t_end=self.__t_end, s=self.__s,
            threads=self.__threads,
            on_data_in_progress=self.__on_data_in_progress, on_data_ready=self.__on_data_ready
        )

    def __on_data_in_progress(self, l_start: int, l_end: int, t: int) -> None:
        self.data_in_progress.emit(l_start, l_end, t)

    def __on_data_ready(self, l_start: int, t: int, data: numpy.ndarray) -> None:
        self.data_ready.emit(l_start, t, data.copy())
